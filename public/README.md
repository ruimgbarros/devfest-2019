# 'Newsroom Hacks'. How data journalism was bootstrapped at Renascença (along with open data)

Slides for my talk about Renascença's data journalism team delivered at [GDG DevFest 2019](https://devfest.gdglisbon.xyz/schedule/2019-12-07?sessionId=229).

Check out the slides at [http://bit.ly/ddj-devfest-2019](http://bit.ly/ddj-devfest-2019)!


Slides created with [remark.js](http://remarkjs.com/) and the R package [**xaringan**](https://github.com/yihui/xaringan)

My xaringan theme (from [xaringanthemer](https://pkg.garrickadenbuie.com/xaringanthemer/)):

```
mono_accent(
    base_color         = "#F48024",
    header_font_google = google_font("IBM Plex Sans", "700"),
    text_font_google   = google_font("IBM Plex Sans Condensed"),
    code_font_google   = google_font("IBM Plex Mono")
    )
```


